# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# defined configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

# defined resources 
resource "azurerm_resource_group" "resource_group_name" {
  name     = "resoruce_group_name" # actual name of the resource group
  location = "West Europe"
  tags = {
    name = "Your Nice Name"
  }
}
