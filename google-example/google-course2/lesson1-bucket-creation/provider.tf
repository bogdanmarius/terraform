# gcp provider

provider "google" {
  credentials = file(var.gcp_project_key)
  project     = var.gcp_project_id
  region      = var.gcp_region
}
