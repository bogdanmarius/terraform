resource "local_file" "pet" {
    filename = "/work/directory/pets.txt"
    content = "we love pets!"
    file_permission = "0700"
}

resource "random_pet" "my-pet" {
    prefix = "Mrs"
    separator = "."
    length = "1"  
}