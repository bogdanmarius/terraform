# create the resource: google storage bucket
resource "google_storage_bucket" "terraform-bucket1" {
  name          = "terraform-bucket1"
  location      = "europe-west9"
  storage_class = "STANDARD"
  labels = {
    "type"   = "bucket"
    "tool"   = "terraform"
    "author" = "your_name_here"
  }

  uniform_bucket_level_access = true

}

# add local file to storage: demo text file
resource "google_storage_bucket_object" "terraform-bucket1-object" {
  name   = "terraform-bucket1-object.txt"
  bucket = google_storage_bucket.terraform-bucket1.name
  source = "demo-file.txt"
}
