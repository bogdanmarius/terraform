## Welcome to the docker-examples repo

The purpose of this repository is to offer templates that can be used in real life scenarios

# Official documentation:
https://developer.hashicorp.com/terraform/tutorials/docker-get-started

# Example:
https://github.com/ChristianLempa/boilerplates/tree/main/terraform/templates/simple-docker-example