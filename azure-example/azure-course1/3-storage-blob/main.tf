# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }

    time = {
      source  = "hashicorp/time"
      version = "0.12.1"
    }
  }
}

# define configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

provider "time" {
  # Configuration options
}

# define resource wait time
resource "time_sleep" "sleep" {
  create_duration = "10s"
}

# define resource settings
resource "azurerm_storage_account" "storage" {
  name                            = "storage"
  resource_group_name             = "resource_group_name"
  location                        = "West Europe"
  account_tier                    = "Standard"
  account_replication_type        = "LRS"
  allow_nested_items_to_be_public = "true" # Allow Blob anonymous access
  tags = {
    name   = "terraform-demo"
    author = "nice@email.tld"
  }

  depends_on = [
    time_sleep.sleep
    ]
}

# define blob storage settings
resource "azurerm_storage_container" "storage_blob" {
  name                  = "storageblob"
  storage_account_name  = "storage"
  container_access_type = "private" # can be also blob for pubic access

  depends_on = [
    azurerm_storage_account.storage, 
    time_sleep.sleep
    ]
}

# upload the sample file
resource "azurerm_storage_blob" "storage_blob_file" {
  name                   = "blobfile.txt"
  storage_account_name   = "storage"
  storage_container_name = "bustorageblob"
  type                   = "Block"
  source                 = "local-sample.txt"

  depends_on = [
    azurerm_storage_account.storage, 
    azurerm_storage_container.storage_blob
    ]
}
