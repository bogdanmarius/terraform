# variables created to be used with this configuration

variable "gcp_region" {
  type        = string
  description = "Region to use for GCP provider"
  default     = "europe-west9"
}

variable "gcp_project" {
  type        = string
  description = "Project to use with this config"
  default     = "your-project-ID"
}

variable "gcp_credentials" {
  type        = string
  description = "Service account credentials to use with the config"
  default     = "service-account-credentials-file.json"
}

variable "gcp_zone" {
  type        = string
  description = "Zone location for created resources"
  default     = "europe-west9-a"
}
