#!/bin/bash

echo "updating the operating system"
apt update
apt upgrade -y

echo "setup ansible repository"
apt install -y software-properties-common
add-apt-repository --yes --update ppa:ansible/ansible

echo "install ansible"
apt update
apt install -y ansible

echo "testing ansible installation"
ansible --version
