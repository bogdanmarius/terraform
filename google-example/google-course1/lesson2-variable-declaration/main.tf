terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.12.0"
    }
  }
}

provider "google" {
  #   project     = "pid-google-terraform-project"
  project = var.project
  #   credentials = file("iam-service-account-generated-key.json")
  credentials = file(var.credentials_file)
  #   region      = "europe-west3"
  region = var.region
  #   zone        = "europe-west3-a"
  zone = var.zone
}

resource "google_compute_instance" "terraform-vm" {
  name         = "terraform-vm-e2-micro"
  machine_type = "e2-micro"
  #   zone         = "europe-west3-a"
  zone = var.zone
#   allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      #   image = "projects/debian-cloud/global/images/debian-12-bookworm-v20241112"
      image = var.os_image
    }
  }

  network_interface {
    network = "default"
    access_config {
      // necessary even if empty
    }
  }
}
