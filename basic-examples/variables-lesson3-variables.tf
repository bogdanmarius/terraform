variable "filename" {
    default = "/work/directory/pets.txt" 
    }
variable "content" {
    default = "Pets are cool and nice!"
}
variable "file_permission" {
    default = "0700"
}
variable "prefix" {
    default = "Mrs"
}
variable "separator" {
    default = "."
}
variable "length" {
    default = "1"
}