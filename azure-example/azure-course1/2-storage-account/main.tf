# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# defined configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

resource "azurerm_storage_account" "storage_account_name1" {
  name                            = "storageaccountname" #actual name of the storage account
  resource_group_name             = "resource_group_name"
  location                        = "West Europe"
  account_tier                    = "Standard"
  account_replication_type        = "LRS"
  allow_nested_items_to_be_public = "false" # Allow Blob anonymous access
  public_network_access_enabled   = "false"
  tags = {
    environment = "terraform-demo"
  }
}
