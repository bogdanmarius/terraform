# this will ask for user input
variable "project" {}

variable "credentials_file" {}

# this will have the default options already defined
variable "region" {
  default = "europe-west3"
  description = "The default GCP region to use for the VM"
  type = string
}

variable "zone" {
  default = "europe-west3-a"
  description = "The default GCP zone to use for the VM"
  type = string
}

variable "os_image" {
  default = "projects/debian-cloud/global/images/debian-12-bookworm-v20241112"
  description = "This defines the operating system used by the VM"
  type = string
}
