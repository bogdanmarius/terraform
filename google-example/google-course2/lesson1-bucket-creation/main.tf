# define terraform provider and version
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.12.0"
    }
  }
}

# bucket file configuration to store a file

resource "google_storage_bucket" "bucket_name1" {
  name = "terraform-bucket1"
  #   location = "EU" # this is a multi region option
  location      = "EUROPE-WEST3" # this a local region option
  storage_class = "STANDARD"
  hierarchical_namespace {
    enabled = false
  }
  labels = {
    name = "terraform-bucket"
    type = "demo-bucket"
  }
}

# make the bucket objects public over the internet
resource "google_storage_object_access_control" "bucket_name1_access" {
  object = google_storage_bucket_object.bucket_name1_object.name
  bucket = google_storage_bucket.bucket_name1.name
  role   = "READER"
  entity = "allUsers"
}

# upload the demo file with public access: index.html

resource "google_storage_bucket_object" "bucket_name1_object" {
  name   = "index.html"
  source = "../lesson1/bucket1/index.html"
  bucket = google_storage_bucket.bucket_name1.name
  metadata = {
    name = "index.html"
  }
}

# create aditional folders with no public access

resource "google_storage_bucket_object" "bucket_name1_object2" {
  name    = "folder1/" # folder name should end with '/'
  content = "empty folder"
  bucket  = google_storage_bucket.bucket_name1.name
  metadata = {
    name = "folder1"
  }
}

resource "google_storage_bucket_object" "bucket_name1_object3" {
  name    = "folder2/" # folder name should end with '/'
  content = "empty folder"
  bucket  = google_storage_bucket.bucket_name1.name
  metadata = {
    name = "folder2"
  }
}
