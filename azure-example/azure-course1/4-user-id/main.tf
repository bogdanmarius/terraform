# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# defined configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

data "azurerm_client_config" "current" {}
data "azuread_user" "current_user" {
  object_id = data.azurerm_client_config.current.object_id

}

resource "azurerm_resource_group" "terraform_demo" {
  name     = "terraform_demo"
  location = "West Europe"
  tags = {
    userCreated = data.azuread_user.current_user.user_principal_name
  }
}

output "object_id" {
  value = data.azurerm_client_config.current.object_id
}

output "user_principal_name" {
  value = data.azuread_user.current_user.user_principal_name
}
