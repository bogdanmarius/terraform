variable "gcp_region" {
  type        = string
  description = "gcp region of the project"
  default     = "europe-west9"
}

variable "gcp_location" {
  type        = string
  description = "gcp location of the k8s cluster"
  default     = "europe-west9-a"
}
