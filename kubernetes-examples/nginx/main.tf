# Kubernetes Provider
terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.29.0"
    }
  }
}

provider "kubernetes" {
  # used minikube deployed on my local vps
  config_path = var.config_path
}

# kubernetes Namespace
resource "kubernetes_namespace" "deploy1" {
  metadata {
    annotations = {
      name = "deploy1"
    }

    labels = {
      mylabel = "deploy1"
    }

    name = "deploy1"
  }
}

# Kubernetes Deployment
resource "kubernetes_deployment" "nginx-webserver" {

  depends_on = [kubernetes_namespace.deploy1]

  metadata {
    name      = "nginx-webserver"
    namespace = "deploy1"
    labels = {
      app = var.app
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = var.app
      }
    }

    template {
      metadata {
        labels = {
          app = var.app
        }
      }

      spec {
        container {
          image = var.image
          name  = var.name

          port {
            container_port = var.container_port
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

#Kubernetes Service
resource "kubernetes_service" "nginx-service" {

  depends_on = [kubernetes_namespace.deploy1]

  metadata {
    name      = "nginx-service"
    namespace = "deploy1"
  }
  spec {
    selector = {
      app = var.app
    }
    port {
      node_port   = var.node_port
      port        = var.port
      target_port = var.target_port
    }

    type = var.type
  }
}
