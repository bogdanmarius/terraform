# Tutorial: https://cloud.google.com/kubernetes-engine/docs/quickstarts/create-cluster-using-terraform

resource "google_compute_network" "web-nginx-k8s" {
  name = "nginx-web-network"

  auto_create_subnetworks  = false
  enable_ula_internal_ipv6 = true
}

resource "google_compute_subnetwork" "web-nginx-k8s" {
  name = "nginx-web-subnetwork"

  ip_cidr_range = "192.85.85.0/24"
  region        = var.gcp_region

  stack_type       = "IPV4_IPV6"
  ipv6_access_type = "INTERNAL" # Change to "EXTERNAL" if creating an external loadbalancer

  network = google_compute_network.web-nginx-k8s.id
  secondary_ip_range {
    range_name    = "services-range"
    ip_cidr_range = "192.168.0.0/24"
  }

  secondary_ip_range {
    range_name    = "pod-ranges"
    ip_cidr_range = "192.168.1.0/24"
  }
}

resource "google_container_cluster" "web-nginx-k8s" {
  name = "nginx-web-autopilot-cluster"
  # Autopilot clusters must be regional clusters.
  location                 = var.gcp_region
  enable_autopilot         = true
  enable_l4_ilb_subsetting = true

  network    = google_compute_network.web-nginx-k8s.id
  subnetwork = google_compute_subnetwork.web-nginx-k8s.id

  ip_allocation_policy {
    stack_type                    = "IPV4_IPV6"
    services_secondary_range_name = google_compute_subnetwork.web-nginx-k8s.secondary_ip_range[0].range_name
    cluster_secondary_range_name  = google_compute_subnetwork.web-nginx-k8s.secondary_ip_range[1].range_name
  }

  # Set `deletion_protection` to `true` will ensure that one cannot
  # accidentally delete this instance by use of Terraform.
  deletion_protection = false
}
