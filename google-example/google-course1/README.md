For GCP lessons in here there are some requirements:
- a GCP account
- billing activate to receive the free amount of money to use for proper testin
- IAM setup just for the terraform application

To setup IAM access for terraform uses the following:
- GCP -> IAM & Admin - Service accounts -> Create Service account
Complete the service accounts details, use easy to spot name and proper description
- once the Key is created click on it and go to KEYS -> download the key file as JSON 
- place the JSON file with the key in the same place as main.tf file for practice.
