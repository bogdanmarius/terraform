# docker host value
variable "host" {
  default = "unix:///var/run/docker.sock"
}
# docker image name and version
variable "name" {
  default = "nginx:latest"
}
# define internal and external ports
variable "internal" {
  default = 80
}
variable "external" {
  default = 9090
}
