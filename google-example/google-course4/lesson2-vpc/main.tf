# create gcp network with automatic setup
resource "google_compute_network" "terraform-lan1-auto" {
  name ="terraform-lan1-auto"
  auto_create_subnetworks = true
}

# create gcp network with manual setup and subnet
resource "google_compute_network" "terraform-lan2-custom" {
  name = "terraform-lan2-custom"
  auto_create_subnetworks = false
}

# create gcp manual subnet with ip cidr range
resource "google_compute_subnetwork" "terraform-subnet1-europe-west9" {
  name = "terraform-subnet1-europe-west9"
  network = google_compute_network.terraform-lan2-custom.id
  region = "europe-west9"
  ip_cidr_range = "10.85.0.0/24"
  private_ip_google_access = true
}

# generate the output of the commands executed via terraform
output "lan1-auto" {
  value = google_compute_network.terraform-lan1-auto.id
}

output "lan2-custom" {
  value = google_compute_network.terraform-lan2-custom.id
}
