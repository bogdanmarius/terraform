# Tutorial : https://cloud.google.com/kubernetes-engine/docs/quickstarts/create-cluster-using-terraform

data "google_client_config" "web-nginx-k8s" {}

provider "kubernetes" {
  host                   = "https://${google_container_cluster.web-nginx-k8s.endpoint}"
  token                  = data.google_client_config.web-nginx-k8s.access_token
  cluster_ca_certificate = base64decode(google_container_cluster.web-nginx-k8s.master_auth[0].cluster_ca_certificate)

  ignore_annotations = [
    "^autopilot\\.gke\\.io\\/.*",
    "^cloud\\.google\\.com\\/.*"
  ]
}

resource "kubernetes_deployment_v1" "web-nginx-k8s" {
  metadata {
    name = "example-nginx-deployment"
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "nginx"
      }
    }

    template {
      metadata {
        labels = {
          app = "nginx"
        }
      }

      spec {

        container {
          image = "nginx:1.27-alpine"
          name  = "nginx-container"

          port {
            container_port = 80
            name           = "nginx-svc"
          }

          # CPU & RAM Limits
          resources {
            requests = {
              cpu    = "250m"  # Request 250 milliCPU (0.25 vCPU or 1 for a CPU core)
              memory = "256Mi" # Request 256 MB RAM could be also 1024 Mi, 2048Mi and so on
            }

            limits = {
              cpu    = "500m"  # Max 500 milliCPU (0.5 vCPU or 1 for a CPU core)
              memory = "512Mi" # Max 512 MB RAM could be also 1024 Mi, 2048Mi and so on
            }
          }

          security_context {
            allow_privilege_escalation = false
            privileged                 = false
            read_only_root_filesystem  = false

            capabilities {
              add  = []
              drop = ["NET_RAW"]
            }
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 80

              http_header {
                name  = "X-Custom-Header"
                value = "Awesome"
              }
            }

            initial_delay_seconds = 3
            period_seconds        = 3
          }
        }

        security_context {
          run_as_non_root = true

          seccomp_profile {
            type = "RuntimeDefault"
          }
        }

        # Toleration is currently required to prevent perpetual diff:
        # https://github.com/hashicorp/terraform-provider-kubernetes/pull/2380
        toleration {
          effect   = "NoSchedule"
          key      = "kubernetes.io/arch"
          operator = "Equal"
          value    = "amd64"
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "web-nginx-k8s" {
  metadata {
    name = "example-nginx-loadbalancer"
    annotations = {
      "networking.gke.io/load-balancer-type" = "Internal" # Remove to create an external loadbalancer
    }
  }

  spec {
    selector = {
      app = kubernetes_deployment_v1.web-nginx-k8s.spec[0].selector[0].match_labels.app
    }

    ip_family_policy = "RequireDualStack"

    port {
      port        = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }

  depends_on = [time_sleep.wait_service_cleanup]
}

# Provide time for Service cleanup
resource "time_sleep" "wait_service_cleanup" {
  depends_on = [google_container_cluster.web-nginx-k8s]

  destroy_duration = "300s"
}
