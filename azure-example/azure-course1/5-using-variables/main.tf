terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# define configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

# define global variables
variable "storage_account_name" {
  type        = string
  description = "Enter the storage account name"
}

# define local variables
locals {
  resource_group_name = "resource_group_name"
  location = "West Europe"
}

# define resource settings
resource "azurerm_storage_account" "storage" {
#   name                            = "storage"
  name                            = var.storage_account_name
#   resource_group_name             = "resource_group_name"
#   location                        = "West Europe"
  resource_group_name = local.resource_group_name
  location = local.location
  account_tier                    = "Standard"
  account_replication_type        = "LRS"
  allow_nested_items_to_be_public = "true" # Allow Blob anonymous access
  tags = {
    name   = "terraform-demo"
    author = "pretty@email-address.tld"
  }
}
