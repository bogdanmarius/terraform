## Kubernetes examples

Example of file separation for each type of resource:
<p>https://github.com/ChristianLempa/boilerplates/tree/main/terraform/kubernetes</p>

## Official Documentation

# terraform example: 
https://developer.hashicorp.com/terraform/tutorials/kubernetes/kubernetes-provider
# terraform provider: 
https://registry.terraform.io/providers/hashicorp/kubernetes/latest