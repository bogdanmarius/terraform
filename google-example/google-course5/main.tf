
resource "google_compute_address" "static" {
  name = "static-apache"
}

resource "google_compute_instance" "terraform-apache" {
  name = "terraform-apache"
  zone = var.gcp_zone
  tags = ["allow-http80", "terraform"]

  machine_type = "e2-micro"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
      labels = {
        name = "terraform-deployment"
      }
    }
  }

  network_interface {
    network = "default"

    access_config {
      nat_ip = google_compute_address.static.address
    }
  }

  metadata_startup_script = file("startup_script.sh")
}

resource "google_compute_firewall" "terraform-firewall" {
  name    = "allow-http-rule"
  network = "default"

  allow {
    ports    = ["80"]
    protocol = "tcp"
  }

  source_tags = ["allow-http80", "terraform"]

  priority = 1000
}

output "public_ip_address" {
  value = google_compute_address.static.address
}
