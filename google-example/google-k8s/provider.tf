terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.19.0"
    }
  }
}

provider "google" {
  project     = "gcp-project-name"
  region      = "eurpe-west9"
  zone        = "europe-west9-a"
  credentials = "project-service-account-credentials.json"
}
