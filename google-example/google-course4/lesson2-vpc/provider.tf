terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.14.1"
    }
  }
}

provider "google" {
  project     = "project-name"
  region      = "europe-west9"
  zone        = "europe-west9-a"
  credentials = "project-name-credentials.json"
}
