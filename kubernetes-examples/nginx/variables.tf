# kubernetes config path
variable "config_path" {
  # i have used minikube on my local vps
  default = "/home/username/.kube/config"
}
# define the app name across the main tf file
variable "app" {
  default = "nginx-web"
}
# define the image name and version
variable "image" {
  default = "nginx:latest"
}
#define the container name
variable "name" {
  default = "nginx-container"
}
#define the container_port
variable "container_port" {
  default = 80
}
#define service variables
variable "node_port" {
  default = 30201
}
variable "port" {
  default = 8090
}
variable "target_port" {
  default = 80
}
variable "type" {
  default = "NodePort"
}
