terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.12.0"
    }
  }
}

provider "google" {
  project     = var.gcp_project
  credentials = var.gcp_credentials
  region      = var.gcp_region
  zone        = var.gcp_zone
}

resource "google_sql_database_instance" "terraform-sql1-instance" {
  name   = "terraform-sql1-instance"
  region = "europe-west3"
  # possible values to use here: SQLSERVER_2019_STANDARD or SQLSERVER_2019_EXPRESS
  database_version = "SQLSERVER_2019_EXPRESS"
  root_password    = "MyTestPassword@2024"
  settings {
    # machine name can be db-custom-1-3840 
    # more details: https://cloud.google.com/sql/docs/mysql/create-instance#machine-types
    tier = "db-custom-1-3840"
    password_validation_policy {
      min_length                  = 10
      complexity                  = "COMPLEXITY_DEFAULT"
      reuse_interval              = 2
      disallow_username_substring = true
      enable_password_policy      = true
    }
  }

  deletion_protection = false
}

resource "google_sql_database" "terraform-sql1-db" {
  name     = "terraform-sql1-db"
  instance = google_sql_database_instance.terraform-sql1-instance.name

  depends_on = [google_sql_database_instance.terraform-sql1-instance]
}

resource "google_sql_user" "terraform-sql1-user" {
  # has default access to the master database
  name     = "terraform-sql1-user"
  instance = google_sql_database_instance.terraform-sql1-instance.name
  password = "MyTestPassword@2024"

  depends_on = [
    google_sql_database_instance.terraform-sql1-instance,
    google_sql_database.terraform-sql1-db
  ]
}
