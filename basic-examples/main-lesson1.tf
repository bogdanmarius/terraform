resource "local_file" "pet" {
    filename = "/work/directory/pets.txt"
    content = "we love pets!"
    file_permission = "0700"
}

resource "local_file" "cat" {
    filename = "/work/directory/cat.txt"
    content = "My favorite pet is Mr. Whiskers"
}