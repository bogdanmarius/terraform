terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.14.1"
    }
  }
}

# variables are defined in variables.tf
provider "google" {
  region      = var.gcp_region
  project     = var.gcp_project
  credentials = var.gcp_credentials
}
