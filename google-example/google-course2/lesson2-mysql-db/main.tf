terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.12.0"
    }
  }
}

provider "google" {
  project     = var.gcp_project
  credentials = var.gcp_credentials
  region      = var.gcp_region
  zone        = var.gcp_zone
}

resource "google_sql_database_instance" "terraform-sql1-instance" {
  name             = "terraform-sql1-instance"
  region           = "europe-west3"
  database_version = "MYSQL_8_0"
  # root_password    = "MyTestPassword@2024"
  settings {
    tier = "db-f1-micro"
    # password_validation_policy {
    #   min_length                  = 10
    #   complexity                  = "COMPLEXITY_DEFAULT"
    #   reuse_interval              = 2
    #   disallow_username_substring = true
    #   enable_password_policy      = true
    # }
  }

  deletion_protection = false
}

resource "google_sql_database" "terraform-sql1-db" {
  name     = "terraform-sql1-db"
  instance = google_sql_database_instance.terraform-sql1-instance.name

  depends_on = [google_sql_database_instance.terraform-sql1-instance]
}

resource "google_sql_user" "terraform-sql1-user" {
  name     = "terraform-sql1-user"
  instance = google_sql_database_instance.terraform-sql1-instance.name
  host     = "%"
  password = "MyTestPassword@2024"

  depends_on = [
    google_sql_database_instance.terraform-sql1-instance,
    google_sql_database.terraform-sql1-db
  ]
}
