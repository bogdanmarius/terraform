# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# defined configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

# defined resources 
resource "azurerm_resource_group" "resource_group_name" {
  name     = "resource_group_name" # actual name of the resource group
  location = "West Europe"
  tags = {
    name = "your name"
  }
}

resource "azurerm_mssql_server" "msqsl-server1" {
  name                         = "mssql-server198514"
  resource_group_name          = azurerm_resource_group.resource_group_name.name
  location                     = azurerm_resource_group.resource_group_name.location
  version                      = "12.0"
  administrator_login          = "sqladmin"
  administrator_login_password = "4-v3ry-53cr37-p455w0rd"
}

resource "azurerm_mssql_database" "mssql-server1-db" {
  name         = "mssql-server1-db"
  server_id    = azurerm_mssql_server.msqsl-server1.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 2
  sku_name     = "S0"
  enclave_type = "VBS"

  depends_on = [
    azurerm_mssql_server.msqsl-server1
  ]

  tags = {
    build = "terraform"
  }

}
