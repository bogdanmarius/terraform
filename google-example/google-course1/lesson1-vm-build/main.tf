terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.12.0"
    }
  }
}

provider "google" {
  project     = "pid-google-terraform-project"
  credentials = file("iam-service-account-generated-key.json")
  region      = "europe-west3"
  zone        = "europe-west3-a"
}

resource "google_compute_instance" "terraform-vm" {
  name         = "terraform-vm-e2-micro"
  machine_type = "e2-micro"
  zone         = "europe-west3-a"

  boot_disk {
    initialize_params {
      image = "projects/debian-cloud/global/images/debian-12-bookworm-v20241112"
    }
  }

  network_interface {
    network    = google_compute_network.terraform-network.self_link
    subnetwork = google_compute_subnetwork.terraform-subnet.self_link
    access_config {
      // necessary even if empty
    }
  }
}

resource "google_compute_network" "terraform-network" {
  name                    = "terraform-network"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "terraform-subnet" {
  name          = "terraform-subnet"
  ip_cidr_range = "10.156.0.0/20" # europe-west3
  region        = "europe-west3"
  network       = google_compute_network.terraform-network.id
}
