terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.15.0"
    }
  }
}

provider "google" {
  project     = "project-name"
  region      = "europe-west9"
  zone        = "europe-west9-a"
  credentials = "project-name-key.json"
}
