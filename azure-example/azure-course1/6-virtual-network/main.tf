# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# defined configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

# define local variables
locals {
  resource_group = "resource_group_name"
  location       = "West Europe"
}

# fetch data about resources
data "azurerm_subnet" "subnet" {
  name = "subnet1"
  virtual_network_name = "vlan1"
  resource_group_name = local.resource_group
}

# defined resources 
resource "azurerm_resource_group" "resource_group_name" {
  name     = local.resource_group # actual name of the resource group
  location = local.location
  tags = {
    name  = "owner name"
    email = "owner@email.tld"
  }
}

resource "azurerm_virtual_network" "vlan1" {
  name                = "vlan1"
  location            = local.location
  resource_group_name = azurerm_resource_group.resource_group_name.name
  address_space       = ["10.1.0.0/16"]
  #   dns_servers         = ["10.0.0.4", "10.0.0.5"]

  subnet {
    name             = "subnet1"
    address_prefixes = ["10.1.1.0/24"]
  }

  tags = {
    name  = "owner name"
    email = "owner@email.tld"
  }
}
