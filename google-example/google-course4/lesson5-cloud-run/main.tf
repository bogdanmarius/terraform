# creating cloud run services
# we create hello app version 1 then verson 2
# after that we apply the traffic limits
resource "google_cloud_run_service" "label1-terraform-cloud-service1" {
  name     = "label1-terraform-cloud-service1"
  location = "europe-west9"

  template {
    spec {
      containers {
        # image = "gcr.io/google-samples/hello-app:1.0"
        image = "gcr.io/google-samples/hello-app:2.0"
      }
    }
  }

  #default traffic setting with newest version
  #   traffic {
  #     percent = 100
  #     latest_revision = true
  #   }


  # the value provided here are extracted from Cloud Run - Service Details - Revisions
  # update the valus as needed
  traffic {
    percent       = 50
    revision_name = "label1-terraform-cloud-service1-00001-rg4"
  }

  traffic {
    percent       = 50
    revision_name = "label1-terraform-cloud-service1-00002-tnf"
  }
}

# we defined the IAM policy to access the cloud run resources
resource "google_cloud_run_service_iam_policy" "label1-terraform-cloud-service1-iam-policy" {
  service     = google_cloud_run_service.label1-terraform-cloud-service1.name
  location    = google_cloud_run_service.label1-terraform-cloud-service1.location
  policy_data = data.google_iam_policy.label1-terraform-cloud-service1-iam-policy.policy_data
}

data "google_iam_policy" "label1-terraform-cloud-service1-iam-policy" {
  binding {
    role    = "roles/run.invoker"
    members = ["allUsers"]
  }
}
