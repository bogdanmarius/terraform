# terraform with azure example

## Using windows 11 with WSL running Ubuntu 24.04

<br><strong>Requirements: 
<p>WSL, running Ubuntu
<p>terraform installed inside WSL
<p>azure-cli installed inside WSL

<br><strong>Execution:
- from WSL linux distro login to azure:
<p>az login -> take the link open it in your browser and login to Azure
<p>az logout -> to logout after you are done with the practice

- save the main.tf on the local WSL system and execute terraform commands on it to deploy the resource group:
    <p>terraform init -> downloads resources as mentioned in the main.tf file
    <p>terraform fmt -> formats the main.tf file to have correct indention
    <p>terraform validate -> checks the resources defined in the main.tf file
    <p>terraform plan -> displays a detailed output of the resources that are about to be build
    <p>terraform apply -> creates the resources defined inside main.tf
- updating the main.tf file requires formatting and validation before we apply the setup
    <p>terraform fmt
    <p>terraform validate
    <p>terraform apply
- to delete the defined resources we use the destroy command:
    <p>terraform destroy