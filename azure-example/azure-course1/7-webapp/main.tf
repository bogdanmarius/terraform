# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# defined configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azire-portal"
  features {}
}

# defined resources 
resource "azurerm_resource_group" "resource_group_name" {
  name     = "resource_group_name" # actual name of the resource group
  location = "West Europe"
  tags = {
    name = "your name"
  }
}

resource "azurerm_app_service_plan" "webapp-plan1" {
  name                = "webapp-plan1"
  location            = azurerm_resource_group.resource_group_name.location
  resource_group_name = azurerm_resource_group.resource_group_name.name

  sku {
    tier = "Standard"
    size = "S1"
  }
}

resource "azurerm_app_service" "webapp-plan1-service" {
  name                = "webapp-plan1-service"
  location            = azurerm_resource_group.resource_group_name.location
  resource_group_name = azurerm_resource_group.resource_group_name.name
  app_service_plan_id = azurerm_app_service_plan.webapp-plan1.id

  site_config {
    dotnet_framework_version = "v6.0"
    scm_type                 = "LocalGit"
  }

}
