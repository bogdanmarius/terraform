# creating the VM
resource "google_compute_instance" "somename1_ubuntu_vm1" {
  name         = "somename1-ubuntu-vm1"
  machine_type = "e2-small"
  zone         = "europe-west9-a"
  tags         = ["somename1", "terraform-build"]

  boot_disk {
    initialize_params {
      image = "projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20250112"
      labels = {
        env     = "dev"
        project = "project-name"
      }
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.somename1_subnet1.id
    network    = google_compute_network.somename1_lan1.id

    access_config {
      nat_ip = google_compute_address.static.address
    }
  }

  # script to install ansible from ubuntu repository
  metadata_startup_script = file("startup-script.sh")

  metadata = {
    build   = "terraform"
    env     = "dev"
    owner   = "email@address.here"
    project = "projectID"
  }

  depends_on = [google_compute_network.somename1_lan1]
}

# creating the VPC
resource "google_compute_network" "somename1_lan1" {
  name                    = "somename1-lan1"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "somename1_subnet1" {
  name                     = "somename1-subnet1"
  network                  = google_compute_network.somename1_lan1.id
  region                   = "europe-west9"
  ip_cidr_range            = "10.85.0.0/24"
  private_ip_google_access = true
  depends_on               = [google_compute_network.somename1_lan1]
}

# creating the router
resource "google_compute_router" "somename1_router1" {
  name       = "somename1-router1"
  region     = "europe-west9"
  network    = google_compute_network.somename1_lan1.id
  depends_on = [google_compute_subnetwork.somename1_subnet1]
}

# creating the router nat
resource "google_compute_router_nat" "somename1_router_nat1" {
  name                               = "somename1-router-nat1"
  router                             = google_compute_router.somename1_router1.name
  region                             = google_compute_router.somename1_router1.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

# assign a static ip address
resource "google_compute_address" "static" {
  name = "somename1-static-ip"
}

# create firewall rules
resource "google_compute_firewall" "somename1_firewall1" {
  name    = "somename1-allow-ssh-rule"
  network = google_compute_network.somename1_lan1.id

  allow {
    ports    = ["22"]
    protocol = "tcp"
  }

  # source_tags If source tags are specified, the firewall will apply only to traffic with source IP that belongs to a tag listed in source tags. 
  # source_tags cannot be used to control traffic to an instance's external IP address. 

  # source_ranges can be 0.0.0.0/0 as wildcard option
  # source_ranges = ["0.0.0.0/0"]

  # google cloud source ranges (including ssh from the browser)
  source_ranges = ["35.235.240.0/20"]

  priority = 1000
}

# generate the output of the network setup
output "somename1_lan1" {
  value = google_compute_network.somename1_lan1.id
}

# output of the public ip address
output "public_ip_address" {
  value = google_compute_address.static.address
}
