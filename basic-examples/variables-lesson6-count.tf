variable "filename" {
    default = [
        "/work/directory/pets.txt", # in relation to count value from main.tf
        "/work/directory/dogs.txt", # in relation to count value from main.tf
        "/work/directory/cats.txt" # in relation to count value from main.tf
    ]
}
variable "content" {
    default = "Pets are cool and nice!"
}
variable "file_permission" {
    default = "0700"
}
variable "prefix" {
    default = "Mrs"
}
variable "separator" {
    default = "."
}
variable "length" {
    default = "1"
}