# defined terraform provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# defined configuration settings for the provider
provider "azurerm" {
  subscription_id = "subscription-id-from-azure-portal"
  tenant_id       = "tenant-id-from-azure-portal"
  features {}
}

# defined resources 
resource "azurerm_resource_group" "resource_group_name" {
  name     = "resource_group_name" # actual name of the resource group
  location = "West Europe"
  tags = {
    name = "owner name"
  }
}

resource "azurerm_virtual_network" "vlan1" {
  name                = "vlan1"
  address_space       = ["10.1.0.0/16"]
  location            = azurerm_resource_group.resource_group_name.location
  resource_group_name = azurerm_resource_group.resource_group_name.name
}

resource "azurerm_subnet" "subnet1" {
  name                 = "subnet1"
  resource_group_name  = azurerm_resource_group.resource_group_name.name
  virtual_network_name = azurerm_virtual_network.vlan1.name
  address_prefixes     = ["10.1.2.0/24"]

  depends_on = [
    azurerm_virtual_network.vlan1
  ]
}

resource "azurerm_network_interface" "vm-lan1" {
  name                = "vm-lan1"
  location            = azurerm_resource_group.resource_group_name.location
  resource_group_name = azurerm_resource_group.resource_group_name.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vm-public-ip.id
  }

  depends_on = [
    azurerm_virtual_network.vlan1,
    azurerm_public_ip.vm-public-ip
  ]
}

resource "azurerm_public_ip" "vm-public-ip" {
  name                = "vmPublicIp"
  resource_group_name = azurerm_resource_group.resource_group_name.name
  location            = azurerm_resource_group.resource_group_name.location
  allocation_method   = "Static"

  tags = {
    build = "terraform-job"
  }

  depends_on = [
    azurerm_resource_group.resource_group_name
  ]
}

resource "azurerm_linux_virtual_machine" "ubuntu-vm" {
  name                            = "ubuntu-vm"
  resource_group_name             = azurerm_resource_group.resource_group_name.name
  location                        = azurerm_resource_group.resource_group_name.location
  size                            = "Standard_F2"
  admin_username                  = "theadminuser"
  admin_password                  = "DemoPass@2024"
  disable_password_authentication = "false"
  network_interface_ids = [
    azurerm_network_interface.vm-lan1.id,
  ]

  #   admin_ssh_key {
  #     username   = "adminuser"
  #     public_key = file("~/.ssh/id_rsa.pub")
  #   }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  depends_on = [
    azurerm_network_interface.vm-lan1
  ]
}
