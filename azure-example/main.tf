# Provides configuration details for Terraform
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.9.0"
    }
  }
}

# Provides configuration details for Azure Terraform provider
provider "azurerm" {
  subscription_id = "azure-subscription_id"
  features {}
}

# Provides the Resource Group to logically contain resources
resource "azurerm_resource_group" "rg" {
  name     = "azure-demo-rg"
  location = "westeurope"
  tags = {
    env     = "dev"
    owner   = "owner name here"
    email   = "email@domain.tld"
    project = "project-name"
    source  = "Terraform"
    type    = "demo"
  }
}
