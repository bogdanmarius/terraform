variable "filename" {
    default = "/home/working/directory/pets.txt" 
    }
variable "content" {
    default = "Pets are cool!"
}
variable "file_permission" {
    default = "0700"
}
variable "prefix" {
    default = "Mrs"
}
variable "separator" {
    default = "."
}
variable "length" {
    default = "1"
}