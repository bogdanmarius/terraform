# create the mysql instance
resource "google_sql_database_instance" "label1-terraform-mysql1" {
  name             = "label1-terraform-mysql1"
  region           = "europe-west9"
  database_version = "MYSQL_8_0"
  settings {
    tier = "db-f1-micro"
  }

  deletion_protection = false
}

# create the mysql database
resource "google_sql_database" "label1-terraform-mysql1-db1" {
  name     = "label1-terraform-mysql1-db1"
  instance = google_sql_database_instance.label1-terraform-mysql1.name
}

# creat the sql user
resource "google_sql_user" "label1-terraform-mysql1-user1" {
  name     = "label1-terraform-mysql1-user1"
  instance = google_sql_database.label1-terraform-mysql1-db1.name
  password = "This1sAT3stPassw0rD"
}
