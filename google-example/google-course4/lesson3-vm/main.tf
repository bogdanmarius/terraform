resource "google_compute_instance" "owner-terraform-vm1" {
  name         = "owner-terraform-vm1"
  machine_type = "e2-medium"
  zone         = "europe-west9-a"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        build = "terraform"
        owner = "owner"
      }
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }

  metadata = {
    foo = "bar"
  }

  metadata_startup_script = "echo hi > /test.txt"

  # allow_stopping_for_update = true
}
